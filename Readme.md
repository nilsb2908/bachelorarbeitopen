Die hier vorgestellte Applikation basiert auf meiner Bachelorarbeit aus meinem Medieninformatik Studium.
Es wurde eine Webapplikation mit Node.js Backend entwickelt, welche ein interaktives 3D-Modell des Haus Grashof der Beuth Hochschule beinhaltet. 
Das 3D-Element verschafft einen interaktiven Überblick über die Etagen und Räume des Gebäudes.