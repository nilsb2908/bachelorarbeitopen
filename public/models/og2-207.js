{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.65 Exporter",
		"vertices"      : 15,
		"faces"         : 9,
		"normals"       : 14,
		"colors"        : 0,
		"uvs"           : [],
		"materials"     : 0,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [],

	"vertices" : [5.88226,1.4,1.63487,4.83186,1.4,1.63487,5.88226,1.4,0.0349987,4.83186,1.4,0.0349987,6.12589,1.4,0.0349987,6.31769,1.4,0.0349987,4.83186,1.4,1.40112,5.88226,1.4,1.40112,5.88226,1.9,1.63487,4.83186,1.9,1.63487,5.88226,1.9,0.0349987,4.83186,1.9,0.0349987,6.12589,1.9,0.0349987,6.31769,1.9,0.0349987,4.83186,1.9,1.40112],

	"morphTargets" : [],

	"normals" : [0.447188,0.894406,0,0,1,0,0,0.707083,0.707083,0.577349,0.577349,0.577349,0,0.894406,0.447188,-0.489059,0.592608,-0.639973,-0.742149,0.639027,-0.201972,0,0.707083,-0.707083,0.707083,0,0.707083,1,0,0,-0.607196,0,-0.794519,-0.964873,0,-0.262612,0,0,1,0,0,-1],

	"colors" : [],

	"uvs" : [[]],

	"faces" : [33,6,7,2,3,0,1,2,3,32,4,0,5,4,5,6,33,0,4,2,7,5,4,2,1,33,1,0,7,6,7,5,1,0,33,6,3,11,14,0,3,8,9,33,5,0,8,13,6,5,10,11,33,2,4,12,10,2,4,12,12,33,3,2,10,11,3,2,12,8,33,0,1,9,8,5,7,13,10],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animations" : []


}
