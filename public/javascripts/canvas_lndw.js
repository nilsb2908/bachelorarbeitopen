/**
 * Created by Nils Brinkmann on 12.12.13.
 */

$(function () {
    var webGL = $("#WebGL-output");
    var accordion = document.getElementById("accordion");
    $('#accordion').accordion({ heightStyle: "content" });
    var searchInput = $('#search-input');
    document.getElementById('search-input').value = "";

    var meshFiles;
    var geometryNameSet;
    var data = new Object();
    var activeRoomName;
    var activeFloorMeshes;
    var nameOfActiveFloor;
    var meshnamesOfActiveFloor = [];
    var meshNameSet = {};

    // WebSocket-Connection to Server
    var socket = io.connect('http://localhost:3000');

    // getting the geometrie-data from server
    socket.on('filesLndw', function(files){
        meshFiles = files;
        loadMeshes();
    });

    // getting the database-data from server
    socket.on('dataLndw', function(d){
        data = d;
        $('#accordion').accordion("refresh");
    });

    // refreshing the accordion panel
    function refreshAccordion(){
        if(searchMode){
            $( "#accordion" ).accordion({
                active: false,
                collapsible: true
            });
            return;
        }
        if(!data){
            alert("no db connection!");
        }
        var activeIndex = $('#accordion').accordion('option', 'active');
        var header = null;
        if(activeIndex){
            header = $("#accordion h3").eq(activeAccordionIndex).text();
        }
        console.log('activeIndex in refresh: ' + activeAccordionIndex);
        console.log('header in refresh: ' + header);
        $('#accordion').empty();
        meshnamesOfActiveFloor = [];

        if(nameOfActiveFloor){
            for(var i=0;i<data.length;i++){
    //            when floor is selected
                if(data[i].stand.substring(0,4) == nameOfActiveFloor){
                    meshnamesOfActiveFloor.push(data[i].stand);
                    var stand = document.createElement('h3');
                    var r = data[i].stand;
                    stand.innerHTML = r.substring(4);
                    var schedule = document.createElement('div');
                    var scheduleData = data[i].schedule;
                    if(scheduleData.length == undefined){
                        for(var k in scheduleData){
                            var p = document.createElement('p');

                            p.innerHTML = k + " : " + scheduleData[k];
                            schedule.appendChild(p);
                        }
                    }else{
                        var p = document.createElement('p');

                        p.innerHTML = scheduleData;
                        schedule.appendChild(p);
                    }
                    document.getElementById("accordion").appendChild(stand);
                    document.getElementById("accordion").appendChild(schedule);
                }
            }
        }else{
            for(var i=0;i<data.length;i++){
                    var stand = document.createElement('h3');
                    var r = data[i].stand;
                    stand.innerHTML = r.substring(4);
                    var schedule = document.createElement('div');
                    var scheduleData = data[i].schedule;
                    if(scheduleData.length == undefined){
                        for(var k in scheduleData){
                            var p = document.createElement('p');

                            p.innerHTML = k + " : " + scheduleData[k];
                            schedule.appendChild(p);
                        }
                    }else{
                        var p = document.createElement('p');

                        p.innerHTML = scheduleData;
                        schedule.appendChild(p);
                    }
                    document.getElementById("accordion").appendChild(stand);
                    document.getElementById("accordion").appendChild(schedule);
            }
        }

        $( "#accordion" ).accordion({
            active: false,
            collapsible: true
        });
        $('#accordion').accordion("refresh");
    }

    // setting the active element in the accordion
    function setAccordionActive(param){
        console.log("activeRoomName: " +activeRoomName);
        if(param == false){
            $( "#accordion" ).accordion( "option", "active", null );
            return;
        }
        var found = false;
        var i = 0;
        if(!searchMode){
            for(i in meshnamesOfActiveFloor){
                if(meshnamesOfActiveFloor[i].substring(4) == activeRoomName.substring(10)){
                    found = true;
                    break;
                }
            }
        }else{
            for(i in foundMeshNames){
                console.log("setAccordionActive search: " + foundMeshNames[i]);
                if(foundMeshNames[i].substring(4) == activeRoomName.substring(10)){
                    found = true;
                    break;
                }
            }
        }
        if(found){
            $( "#accordion" ).accordion( "option", "active", parseInt(i) );
        }else{
            $( "#accordion" ).accordion( "option", "active", null );
        }
    }

    // search rooms from input field
    var foundMeshNames =[];
    function search(event){
        if(event.keyCode == 13){
            if(document.getElementById('search-input').value.trim() == ""){
                document.getElementById("reset").click();
                return;
            }
            document.getElementById("buildingBread").click();
            var searchString = document.getElementById('search-input').value.toLowerCase();
            if(searchMode){
                document.getElementById("reset").click();
                document.getElementById('search-input').value = searchString;
            }
            console.log(searchString);

            if(!data){
                alert("no db connection!");
            }
            $('#accordion').empty();
            var found = false;
            foundMeshNames =[];
            for(var i=0;i<data.length;i++){
                found = false;
                if((data[i].stand.substring(4)).toLowerCase().indexOf(searchString) != -1){
                    found = true;
                }else{
                    var schedData = data[i].schedule;
                    if(schedData.length == undefined){
                        for(var k in schedData){
                            if(schedData[k].toLowerCase().indexOf(searchString) != -1){
                                found = true;
                                break;
                            }
                        }
                    }else{
                        if(schedData.toLowerCase().indexOf(searchString) != -1){
                            found = true;
                        }
                    }
                }
                if(found){
                    var stand = document.createElement('h3');
                    var r = data[i].stand;
                    foundMeshNames.push(r);
                    stand.innerHTML = r.substring(4);
                    var schedule = document.createElement('div');
                    var scheduleData = data[i].schedule;
                    if(scheduleData.length == undefined){
                        for(var k in scheduleData){
                            var p = document.createElement('p');

                            p.innerHTML = k + " : " + scheduleData[k];
                            schedule.appendChild(p);
                        }
                    }else{
                        var p = document.createElement('p');

                        p.innerHTML = scheduleData;
                        schedule.appendChild(p);
                    }
                    document.getElementById("accordion").appendChild(stand);
                    document.getElementById("accordion").appendChild(schedule);
                }
            }
            $( "#accordion" ).accordion({
                active: false,
                collapsible: true
            });
            $('#accordion').accordion("refresh");

            if(foundMeshNames.length > 0){
                showSearchedMeshes(foundMeshNames);
            }
        }
    }

    var heightScale =  window.innerHeight/window.innerWidth;

    var loader = new THREE.JSONLoader();

//    var stats = initStats();
    var projector = new THREE.Projector();
    var raycaster = new THREE.Raycaster();

    var clickableObjects = [];
    var allObjects = [];
    var selectedMesh, hoveredMesh, hoveredRoom;
    var notClickable = [ "eg1-01", "og1-floor"];

    var roomNames = [];
    var floorSet = {};

    var mouse = new THREE.Vector2();

    // create a scene, that will hold all elements
    var scene = new THREE.Scene();
    setScene(scene);

    // create a camera
    var camera = new THREE.PerspectiveCamera(45, (webGL.width() ) / (webGL.width() * heightScale), 0.1, 1000);
    // position and point the camera to the center of the scene
    camera.position.x = 24.7;
    camera.position.y = 19.8;
    camera.position.z = 68.1;
//    scene.position.x +=10;
    setCamera(camera);
    camera.lookAt(defaultRoomLookAt);

    // create a renderer and set the size
    var renderer;
    if( Detector.webgl ){
        renderer = new THREE.WebGLRenderer({
            antialias                : true       // to get smoother output
        });
        renderer.setClearColor(0xffffff, 1.0);
    }else{
        Detector.addGetWebGLMessage();
        return true;
    }
    renderer.setSize((webGL.width() ), (webGL.width() * heightScale));
    renderer.shadowMapEnabled = true;
    renderer.sortObjects = false;

    var frustum = new THREE.Frustum();
    frustum.setFromMatrix( new THREE.Matrix4().multiply( camera.projectionMatrix, camera.matrixWorldInverse ) );

    // add controls
    var controls = new THREE.OrbitControls(camera, renderer.domElement, scene);
    controls.noKeys = true;
    controls.noPan = false;

    // axis helper
    var axes = new THREE.AxisHelper( 20 );
//    scene.add(axes);

    // create light sources
    var pointLight = new THREE.PointLight(0x555555);
    pointLight.position.set(10,40,60);
    scene.add(pointLight);
    var pointLight2 = new THREE.PointLight(0x555555);
    pointLight2.position.set(-40,20,40);
    scene.add(pointLight2);
    var pointLight3 = new THREE.PointLight(0x777777);
    pointLight3.position.set(-10,40,-30);
    scene.add(pointLight3);

    var directionalLight = new THREE.SpotLight(0x111111);
    directionalLight.position.set(25,40,-10);
    var target = new THREE.Object3D();
    target.position = new THREE.Vector3(0,0,0);
    directionalLight.target = target;
    directionalLight.castShadow = true;
    directionalLight.onlyShadow = true;
    directionalLight.shadowDarkness = 0.6;
    directionalLight.shadowCameraVisible = false;

    scene.add(directionalLight);

    // create an basic material
    var basicMaterial = new THREE.MeshPhongMaterial({
        color: 0x0098A1,
//        color: 0x222222,
        transparent: true,
        opacity: 1,
        side: THREE.DoubleSide,
        shading: THREE.FlatShading
    });
    // create a red material
    var basicMaterialRed = new THREE.MeshPhongMaterial({
        color: 0xEF181E,
        transparent: true,
        opacity: 1,
        side: THREE.DoubleSide,
        shading: THREE.FlatShading
    });

    // ctreate a mesh object, add a material
    function createMesh(name, geometry, clickable, colorRed){
        if(clickable == undefined){
            clickable = true;
        }
        var mesh;
        if(colorRed){
            mesh = new THREE.Mesh(geometry, basicMaterialRed.clone());
        }else{
            mesh = new THREE.Mesh(geometry, basicMaterial.clone());
        }
        mesh.scale.x = 3;
        mesh.scale.y = 3;
        mesh.scale.z = 3;
        mesh.position.x = 8;
        mesh.receiveShadow = false;
        mesh.castShadow = true;
        scene.add(mesh);
        if(clickable){
            clickableObjects.push(mesh);
//            mesh.geometry.computeBoundingBox();
//            console.log(JSON.stringify(mesh.geometry.boundingBox));
//            clickableObjects.push(mesh.geometry.boundingBox);
        }
        allObjects.push(mesh);
        if(allObjects.length == meshFiles.length){
            console.log("all files loaded");
            for(var i in roomNames){
                var floor = roomNames[i].substring(0,4);
                if(floorSet[floor]){
                    floorSet[floor].push(allObjects[i]);
                }else{
                    floorSet[floor] = [];
                    floorSet[floor].push(allObjects[i]);
                }
                meshNameSet[roomNames[i]] = allObjects[i];
            }
//            roomNames[allObjects.indexOf(selectedMesh)];
            setObjRooms(allObjects, roomNames, floorSet, meshNameSet);
            refreshAccordion();
        }
    }

    // iterate over all JSON-files
    function loadMeshes(){

        for(var i=0;i<meshFiles.length;i++){
            var isStand = false;
            if(meshFiles[i].indexOf("stand") >= 0 ){
                var mesh = "models_lndw/" + meshFiles[i];
                isStand = true;
            }else{
                var mesh = "models/" + meshFiles[i];
            }
            var tmp = meshFiles[i].split('.');
            var name = tmp[0];
            roomNames.push(name);
            if(!isStand){
                loader.load(mesh, function(geometry) {
                    createMesh("", geometry, false, false);
                });
            }else{
                loader.load(mesh, function(geometry) {
                    createMesh("", geometry, true, true);
                });
            }
        }
    }

    // adding the EventListeners
    var activeAccordionIndex;
    var globalSelect = false;
    document.getElementById('WebGL-output').addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.getElementById('WebGL-output').addEventListener( 'mouseup', onDocumentMouseUp, false );
    document.getElementById('WebGL-output').addEventListener( 'mousemove', onDocumentMouseMove, false );
    window.addEventListener( 'resize', onWindowResize, false );
    document.getElementById('search-input').addEventListener( 'keyup', search, false );
    $( "#accordion" ).on( "accordionbeforeactivate", function( event, ui ) {
        if(getMode() == modeFloorSelect){
            var headerText = ui.newHeader.text();
            if(!headerText){
                return;
            }
            for(var i in roomNames){
                if(roomNames[i].substring(10) == headerText){
                    if(nameOfActiveFloor==null || nameOfActiveFloor==undefined){
                        console.log("before: "+ui.newHeader.text());
                        onDocumentMouseUp("open", allObjects[i]);
                        globalSelect = true;
                        console.log("global");
                    }
                }
            }
        }else{
            if( ui.newHeader.text() != ""){
                console.log("tab opened: " + ui.newHeader.text());
                var headerText = ui.newHeader.text();
                for(var i in roomNames){
                    if(roomNames[i].substring(10) == headerText){
                        if(nameOfActiveFloor == roomNames[i].substring(0, 4)){
                            onDocumentMouseUp("open", allObjects[i]);
                        }else if(nameOfActiveFloor==null || nameOfActiveFloor==undefined){
                            onDocumentMouseUp("open", allObjects[i]);
                        }
                    }
                }
            }else{
                console.log("tab closed");
//                deselectMesh(selectedMesh, allObjects);
//                hoverOut(selectedMesh);
//                selectedMesh = null;
                var headerText = ui.oldHeader.text();
                console.log(headerText);
                for(var i in roomNames){
                    if(roomNames[i].substring(10) == headerText){
                        onDocumentMouseUp("close", allObjects[i]);
                    }
                }
//                onDocumentMouseUp(null, null);
                activeAccordionIndex = null;
            }
        }
    });
    $( "#accordion" ).on( "accordionactivate", function( event, ui ) {
        var check = parseInt($(this).accordion('option', 'active'));
        if(check >= 0 && !globalSelect){
        }else{
            $("#accordion h3").removeClass('ui-state-focus');
        }
    });

    // refresh the camera and renderer size on resize of the browser window
    function onWindowResize() {
        heightScale =  window.innerHeight/window.innerWidth;
        $('#sidebar').height($('canvas').height());

        camera.aspect = (webGL.width()) / (webGL.width() * heightScale);
        camera.updateProjectionMatrix();

        renderer.setSize( (webGL.width()), (webGL.width() * heightScale) );
    }

    var mouseTmp = new THREE.Vector2();
    function onDocumentMouseDown(event){
        mouseTmp.x = ( (event.clientX - webGL.offset().left) / (webGL.width()) ) * 2 - 1;
        mouseTmp.y = - ( (event.clientY - webGL.offset().top) / (webGL.width() * heightScale) ) * 2 + 1;
    }

    // on release of mouse button
    var clickPoint;
    function onDocumentMouseUp( event, mesh ) {
        if(event && !mesh){
            // mouse position + offset if canvas is not at pos 0,0
            mouse.x = ( (event.clientX - webGL.offset().left) / (webGL.width()) ) * 2 - 1;
            mouse.y = - ( (event.clientY - webGL.offset().top) / (webGL.width() * heightScale) ) * 2 + 1;

            if(mouse.x == mouseTmp.x && mouse.y == mouseTmp.y){
                event.preventDefault();
                var vector = new THREE.Vector3( mouse.x, mouse.y, 0.5 );

                projector.unprojectVector( vector, camera );
                raycaster.set( camera.position, vector.sub( camera.position ).normalize() );

                if(getMode() == modeRoomSelect){
                    var clickIntersects = raycaster.intersectObjects( clickableObjects, true );
                    if ( clickIntersects.length > 0 ) {
                        if(isMeshClickable(clickIntersects[ 0 ].object)){
                            if(selectedMesh == clickIntersects[ 0 ].object){
                                deselectMesh(selectedMesh, allObjects);
                                hoverOut(selectedMesh);
                                selectedMesh = null;
//                                refreshAccordion();
                                setAccordionActive(false);
                                document.getElementById("breadcrumbs").removeChild(room);
                                document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
                                globalSelect = false;
                            }else{
                                if(selectedMesh) {
                                    deselectMesh(selectedMesh, allObjects);
                                    hoverOut(selectedMesh);
                                    document.getElementById("breadcrumbs").removeChild(room);
                                    document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
                                }
                                selectedMesh = clickIntersects[ 0 ].object;
                                clickPoint = clickIntersects[0].point;
                                activeRoomName = roomNames[allObjects.indexOf(selectedMesh)];
                                console.log(activeRoomName);
                                addBreadcrumb("room", activeRoomName);
                                setAccordionActive();
                                selectMesh(selectedMesh, allObjects, clickPoint);
                            }
                        }
                    }else if( clickIntersects.length == 0 ){

                    }
                }else if(getMode() == modeFloorSelect){
                    var clickIntersects = raycaster.intersectObjects( allObjects, true );
                    if ( clickIntersects.length > 0 ) {
                        //                activeRoomName = roomNames[allObjects.indexOf(clickIntersects[ 0 ].object)];
                        if(isFloorClickable(clickIntersects[ 0 ].object)){
                            for(var x in floorSet){
                                if(floorSet[x].indexOf(clickIntersects[ 0 ].object) >= 0){
                                    activeFloorMeshes = floorSet[x];
                                    nameOfActiveFloor = x;
                                    break;
                                }
                            }
                            refreshAccordion();
                            addBreadcrumb("floor", nameOfActiveFloor);
                            selectMesh(clickIntersects[ 0 ].object, allObjects, clickIntersects[0].point);
                        }
                    }
                }
            }
        }else if(mesh != selectedMesh && event == "open"){
            console.log("open");
            if(getMode() == modeFloorSelect){
                for(var x in floorSet){
                    if(floorSet[x].indexOf(mesh) >= 0){
                        activeFloorMeshes = floorSet[x];
                        nameOfActiveFloor = x;
                        break;
                    }
                }
                addBreadcrumb("floor", nameOfActiveFloor);
                selectMesh(mesh, allObjects, true);
                refreshAccordion();

                setTimeout(function(){
                    setMode(modeRoomSelect);
                    selectedMesh = mesh;
                    clickPoint = getCenter(mesh);
                    activeRoomName = roomNames[allObjects.indexOf(selectedMesh)];
                    console.log(activeRoomName);
                    addBreadcrumb("room", activeRoomName);
                    setAccordionActive();
                    selectMesh(selectedMesh, allObjects, clickPoint);
                }, 0);
//                $( "#accordion" ).accordion({
//                    active: activeAccordionIndex,
//                    collapsible: true
//                });
//                $('#accordion').accordion("refresh");
            }else if(getMode() == modeRoomSelect){
                if(selectedMesh) {
                    deselectMesh(selectedMesh, allObjects);
                    hoverOut(selectedMesh);
                    document.getElementById("breadcrumbs").removeChild(room);
                }
                selectedMesh = mesh;
                clickPoint = getCenter(mesh);
                clickPoint.x += selectedMesh.position.x;
                clickPoint.y += selectedMesh.position.y;
                clickPoint.z += selectedMesh.position.z;
                activeRoomName = roomNames[allObjects.indexOf(selectedMesh)];
                console.log(activeRoomName);
                addBreadcrumb("room", activeRoomName);
                if(getMode() == "room"){
                    setAccordionActive();
                }
                hoverOver(selectedMesh);
                selectMesh(selectedMesh, allObjects, clickPoint);
            }
        }else if(mesh == selectedMesh && event == "close"){
            deselectMesh(selectedMesh, allObjects);
            hoverOut(selectedMesh);
            selectedMesh = null;
            document.getElementById("breadcrumbs").removeChild(room);

        }
    }

    // if mouse cursor was moved
    function onDocumentMouseMove( event ) {

        event.preventDefault();

        // mouse position + offset if canvas is not at pos 0,0
        mouse.x = ( (event.clientX - webGL.offset().left) / (webGL.width()) ) * 2 - 1;
        mouse.y = - ( (event.clientY - webGL.offset().top) / (webGL.width() * heightScale) ) * 2 + 1;

        // find intersections for mouseOver
        var vector = new THREE.Vector3( mouse.x, mouse.y, 0 );

        projector.unprojectVector( vector, camera );
        raycaster.set( camera.position, vector.sub( camera.position ).normalize() );
        var mouseIntersects = [];
        if(getMode() == "room"){
            mouseIntersects = raycaster.intersectObjects( clickableObjects, false );

        }else if(getMode() == "floor"){
            mouseIntersects = raycaster.intersectObjects( allObjects, false );
        }else if(getMode() == modeSearch){
            mouseIntersects = raycaster.intersectObjects( clickableObjects, false );
        }

        if ( mouseIntersects.length > 0 ) {
            if ( hoveredMesh != mouseIntersects[ 0 ].object ) {
                if ( hoveredMesh ){
                    if(hoveredMesh != selectedMesh)
                        hoverOut(hoveredMesh);
                    for(var i = 0; i < $("#accordion h3").length; i++){
                        if($("#accordion h3").eq(i).text() == hoveredRoom){
                            $("#accordion h3").eq(i).removeClass('ui-state-hover');
                        }
                    }
                }

                hoveredMesh = mouseIntersects[ 0 ].object;
                hoverOver(hoveredMesh);
                hoveredRoom = roomNames[allObjects.indexOf(mouseIntersects[ 0 ].object)].substring(10);
                for(var i = 0; i < $("#accordion h3").length; i++){
                    if($("#accordion h3").eq(i).text() == hoveredRoom){
                        $("#accordion h3").eq(i).addClass('ui-state-hover');
                    }
                }
            }
        } else {
            if ( hoveredMesh ){
                if(hoveredMesh != selectedMesh)
                    hoverOut(hoveredMesh);
                for(var i = 0; i < $("#accordion h3").length; i++){
                    if($("#accordion h3").eq(i).text() == hoveredRoom){
                        $("#accordion h3").eq(i).removeClass('ui-state-hover');
                    }
                }
            }

            hoveredMesh = null;
        }
    }

    // adding the breadcrumbs to the 3D object
    var breadcrumbs = document.getElementById("breadcrumbs");
    var room, floor, building;
    function addBreadcrumb(mode, name){
        console.log("addBreadcumb, mode = "+mode);
        if(mode == modeRoomSelect){
            console.log("addBread room");
            if(!document.getElementById("floorToRoomArrow")){
                var floorToRoomArrow = document.createElement('span');
                floorToRoomArrow.id = "floorToRoomArrow";
                floorToRoomArrow.appendChild(document.createTextNode(' > '));
                document.getElementById("breadcrumbs").appendChild(floorToRoomArrow);
            }

            room = document.createElement('button');
            room.id = "roomBread";
            room.className = "btn btn-default";
            room.appendChild(document.createTextNode(name.substring(10)));
            document.getElementById("breadcrumbs").appendChild(room);
            document.getElementById("roomBread").addEventListener("click", function () {

            }, false);
        }else if(mode == modeFloorSelect){
            console.log("addBread floor");
            var buildingToFloorArrow = document.createElement('span');
            buildingToFloorArrow.id = "buildingToFloorArrow";
            buildingToFloorArrow.appendChild(document.createTextNode(' > '));
            document.getElementById("breadcrumbs").appendChild(buildingToFloorArrow);

            floor = document.createElement('button');
            floor.id = "floorBread";
            floor.className = "btn btn-default";
            floor.appendChild(document.createTextNode(name.substring(0,name.length-1).toUpperCase()));
            document.getElementById("breadcrumbs").appendChild(floor);
            document.getElementById("floorBread").addEventListener("click", function () {
                deselectMesh(selectedMesh, allObjects);
                hoverOut(selectedMesh);
                selectedMesh = null;
                refreshAccordion();
                if(document.getElementById("roomBread")){
                    document.getElementById("breadcrumbs").removeChild(document.getElementById("roomBread"));
                    document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
                }
            }, false);
        }else if(mode == modeBuildingSelect){
            building = document.createElement('button');
            building.id = "buildingBread";
            building.className = "btn btn-default";
            building.appendChild(document.createTextNode(name));
            document.getElementById("breadcrumbs").appendChild(building);
            document.getElementById("buildingBread").addEventListener("click", function () {
                if(selectedMesh){
                    deselectMesh(selectedMesh, allObjects);
                    hoverOut(selectedMesh);
                    selectedMesh = null;
                    document.getElementById("breadcrumbs").removeChild(room);
                }
                setMode(modeFloorSelect);
                deselectMesh(nameOfActiveFloor, allObjects);
                refreshAccordion();
                if(document.getElementById("floorBread")){
                    document.getElementById("breadcrumbs").removeChild(document.getElementById("floorBread"));
                    document.getElementById("breadcrumbs").removeChild(document.getElementById("buildingToFloorArrow"));
                }
            }, false);
        }
    }

    $(function(){
        console.log("adding first breadcrumb");
        building = document.createElement('button');
        building.id = "buildingBread";
//        building.type = "button";
        building.className = "btn btn-default";
        building.appendChild(document.createTextNode("Grashof"));
        document.getElementById("breadcrumbs").appendChild(building);
        document.getElementById("buildingBread").addEventListener("click", function () {
            if(selectedMesh){
                deselectMesh(selectedMesh, allObjects);
                hoverOut(selectedMesh);
                selectedMesh = null;
                document.getElementById("breadcrumbs").removeChild(document.getElementById("roomBread"));
                document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
            }
            setMode(modeFloorSelect);
            deselectMesh(nameOfActiveFloor, allObjects);
            nameOfActiveFloor = null;
            refreshAccordion();
            if(document.getElementById("floorBread")){
                document.getElementById("breadcrumbs").removeChild(document.getElementById("floorBread"));
                document.getElementById("breadcrumbs").removeChild(document.getElementById("buildingToFloorArrow"));
            }
        }, false);
    });

    function renderScene(){
        requestAnimationFrame(renderScene);
        camera.updateProjectionMatrix();
        controls.update();
//        stats.update();
//        hoverCheck();
        TWEEN.update();

        renderer.render(scene, camera);
    }

    function initStats(){
        var stats = new Stats();
        stats.setMode(0);
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';
        $("#Stats-output").append(stats.domElement);
        return stats;
    }

    // add the output of the renderer to the html element
    $("#WebGL-output").append(renderer.domElement);
    renderScene();
    $('#sidebar').height(window.innerHeight-100);

    $(function(){
        document.getElementById("reset").addEventListener("click", function () {
            if(searchMode){
                document.getElementById("buildingBread").click();
                var mat = {op: 0.1};
                var tweenSelect = new TWEEN.Tween(mat)
                    .to({op: 1}, opacitySpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        for(var i in allObjects){
                            allObjects[i].material.opacity = mat.op;
                        }
                    }).start();
                var staende = getStandMeshes();
               console.log("stande.length"+staende.length);
                for(var l in staende){
                    staende[l].material = basicMaterialRed.clone();
                }
                setSearchMode(false);
                foundMeshNames = [];
                document.getElementById('search-input').value = "";
                refreshAccordion();
            }
        }, false);
    });
});