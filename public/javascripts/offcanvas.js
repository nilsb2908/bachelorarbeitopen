//$(document).ready(function() {
//  $('[data-toggle=offcanvas]').click(function() {
//    $('.row-offcanvas').toggleClass('active');
//  });
//});

$(document).ready(function() {
    $('[data-toggle=offcanvas]').click(function() {
        $('.sidebar-offcanvas').toggleClass('active');
        $('#toggle').toggleClass('active');
    });
});

// set accordion panel so nothing is default collapsed
$(function() {
    $( "#accordion" ).accordion({
        active: false,
        collapsible: true
    });
});

// setting the menu item active
$(document).ready(function($){
    var url = window.location.href;
    var urlSplit = url.split("/");
    console.log("url: " + urlSplit[urlSplit.length-1]);
    $('.nav a[href="/'+urlSplit[urlSplit.length-1]+'"]').parent().addClass('active');
});