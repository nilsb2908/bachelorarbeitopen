/**
 * Created by Nils Brinkmann on 12.12.13.
 */

var scene;

var cameraCopy;
var cameraPositionBuilding = { };
var cameraPositionFloorSelect = { x: 24.7, y: 19.8, z: 68.1 };
var cameraPositionRoomSelect = { x: 22, y: 45, z: 49 };

var modeBuildingSelect = "building";
var modeFloorSelect = "floor"
var modeRoomSelect = "room";
var currentMode = modeFloorSelect;
var searchMode = false;

var maxHoverHeight = 0.7;
var nonSelectedOpacity = 0.2;
var nonSelectedFloorOpacity = 0.3;
var hoverSpeed = 500;
var opacitySpeed = 500;
var allObjects, roomNames, floorSet = [], meshNameSet, selectedFloorMeshes = [];

var defaultLookAt = {x: 0, y: 10, z: 0};
var defaultFoV;

var isMeshSelected = false;
var inTween = false;

var tweenHoverOver, tweenHoverOver2, tweenHoverOut, tweenHoverOut2, tweenSelect, tweenDeselect;

var searchedMeshes = [];
var searchedFloors = [];

// camera tweens
var tweenLookAt, tweenFov, tweenCamPos;

function setScene(sc){
    scene = sc;
}

function isMeshClickable(mesh){
    if(searchMode && searchedMeshes.indexOf(mesh) == -1){
        return false;
    }
    return true;
}

function isFloorClickable(mesh){
    if(searchMode && searchedFloors.indexOf(mesh) == -1){
        return false;
    }
    return true;
}

function setObjRooms(objects, rooms, fset, rset){
    allObjects = objects;
    roomNames = rooms;
    floorSet = fset;
    meshNameSet = rset;
}

// setting the camera from the scene
function setCamera(cam){
    if(!cameraCopy){
        cameraCopy = cam;
        defaultFoV = cameraCopy.clone().fov;
    }
    if(currentMode == modeRoomSelect){
        cameraCopy.position.x = cameraPositionRoomSelect.x;
        cameraCopy.position.y = cameraPositionRoomSelect.y;
        cameraCopy.position.z = cameraPositionRoomSelect.z;
    }else if(currentMode == modeFloorSelect){
        cameraCopy.position.x = cameraPositionFloorSelect.x;
        cameraCopy.position.y = cameraPositionFloorSelect.y;
        cameraCopy.position.z = cameraPositionFloorSelect.z;
        scene.position.y += 10;
    }else if(currentMode == modeBuildingSelect){

    }
}

function setMode(mode){
    currentMode = mode;
}

function getMode(){
    return currentMode;
}

function hoverOver(mesh){
    if(mesh && !inTween){
//    if(mesh ){
        console.log("hoverOver");
        if(currentMode == modeRoomSelect){
            if(selectedFloorMeshes.indexOf(mesh) < 0){
                return;
            }
            var pos = {y: mesh.position.y};
            if(!searchMode){
                    tweenHoverOver = new TWEEN.Tween(pos)
                        .to({y: maxHoverHeight}, hoverSpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            mesh.position.y = pos.y;
                        });
        //            setTimeout('tweenHoverOver.start()', 500);
                    tweenHoverOver.start();
            }else{
                if(searchedMeshes.indexOf(mesh) >= 0){
                    tweenHoverOver = new TWEEN.Tween(pos)
                        .to({y: maxHoverHeight}, hoverSpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            mesh.position.y = pos.y;
                        });
                    //            setTimeout('tweenHoverOver.start()', 500);
                    tweenHoverOver.start();
                }
            }
        }else if(currentMode == modeFloorSelect){
            if(searchMode && searchedFloors.indexOf(mesh) == -1){
                return;
            }
            var hoveredFloor;
            var nonHoveredFloors = {};
            var nonHoveredMesh;
            for(var x in floorSet){
                if(floorSet[x].indexOf(mesh) >= 0){
                    hoveredFloor = floorSet[x];
                }else{
                    nonHoveredFloors[x] = floorSet[x];
                    nonHoveredMesh = nonHoveredFloors[x][1];
                }
            }
            tweenHoverOver = new TWEEN.Tween(mesh.scale)
                .to({x: 3.05, y: 3, z: 3.05}, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in hoveredFloor){
                        hoveredFloor[y].scale = mesh.scale;
                    }
                }).start();
            if(!searchMode){
                var mat = {op: nonHoveredMesh.material.opacity};
//                var mat = {op: 1};
                var matNew = {op: nonSelectedFloorOpacity};
            }else{
                var mat = {op: nonSelectedFloorOpacity};
                var matNew = {op: 0.1};
            }
            new TWEEN.Tween(mat)
                .to(matNew, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonHoveredFloors){
                        for(var z in nonHoveredFloors[y]){
                            if(!searchMode){
//                                if(nonHoveredFloors[y][z].material.opacity > mat.op)
                                    nonHoveredFloors[y][z].material.opacity = mat.op;
                            }else if(searchedMeshes.indexOf(nonHoveredFloors[y][z]) == -1){
                                if(nonHoveredFloors[y][z].material.opacity > mat.op)
                                    nonHoveredFloors[y][z].material.opacity = mat.op;
                            }
                        }
                    }
                } ).start();
        }
    }
}

function hoverOut(mesh){
    if(mesh && !inTween){
        if(currentMode == modeRoomSelect){
            if(selectedFloorMeshes.indexOf(mesh) < 0){
                return;
            }
            var pos = {y: mesh.position.y};
            if(!searchMode){
                tweenHoverOut = new TWEEN.Tween(pos)
                    .to({y: 0}, hoverSpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        mesh.position.y = pos.y;
                    } );

                tweenHoverOut.start();
            }else{
                if(searchedMeshes.indexOf(mesh) >= 0){
                    tweenHoverOut = new TWEEN.Tween(pos)
                        .to({y: 0}, hoverSpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            mesh.position.y = pos.y;
                        } );

                    tweenHoverOut.start();
                }
            }
        }else if(currentMode == modeFloorSelect){
            if(searchMode && searchedFloors.indexOf(mesh) == -1){
                return;
            }
            var hoveredFloor;
            var nonHoveredFloors = {};
            var nonHoveredMesh;
            for(var x in floorSet){
                if(floorSet[x].indexOf(mesh) >= 0){
                    hoveredFloor = floorSet[x];
                }else{
                    nonHoveredFloors[x] = floorSet[x];
                    nonHoveredMesh = nonHoveredFloors[x][1];
                }
            }
            tweenHoverOut = new TWEEN.Tween(mesh.scale)
                .to({x: 3, y: 3, z: 3}, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in hoveredFloor){
                        hoveredFloor[y].scale = mesh.scale;
                    }
                }).start();
            if(!searchMode){
                var matNew = {op: 1};
//                var matNew = {op: nonHoveredMesh.material.opacity};
                var mat = {op: nonSelectedFloorOpacity};
            }else{
                var matNew = {op: nonSelectedFloorOpacity};
                var mat = {op: 0.1};
            }
            tweenHoverOut2 = new TWEEN.Tween(mat)
                .to(matNew, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonHoveredFloors){
                        for(var z in nonHoveredFloors[y]){
                            if(!searchMode){
//                                if(nonHoveredFloors[y][z].material.opacity < mat.op)
                                    nonHoveredFloors[y][z].material.opacity = mat.op;
                            }else if(searchedMeshes.indexOf(nonHoveredFloors[y][z]) == -1){
                                nonHoveredFloors[y][z].material.opacity = mat.op;
                            }
                        }
                    }
                } );
            tweenHoverOut2.start();
        }
    }
}

function selectMesh(mesh, objects, clickPos){
    if(mesh && objects && clickPos){
        console.log("select");
        if(currentMode == modeRoomSelect){
            if(!isMeshSelected){
                if(!searchMode){
                    var mat = {op: mesh.material.opacity};
                    if(mat.op < 1){
                        tweenSelect = new TWEEN.Tween(mat)
                            .to({op: 1}, opacitySpeed)
                            .easing(TWEEN.Easing.Quadratic.Out)
                            .onUpdate( function (){
                                mesh.material.opacity = mat.op;
                            } );
                        tweenSelect.start();
                    }
                    tweenSelect = new TWEEN.Tween(mat)
                        .to({op: nonSelectedOpacity}, opacitySpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            for(var i=0;i<objects.length;i++){
                                if(objects[i] != mesh)
                                    objects[i].material.opacity = mat.op;
                            }
                        } );
                    tweenSelect.start();
                }
                cameraLookAt(getCenter(mesh));
                cameraFov(true);
                isMeshSelected = true;
            }
        }else if(currentMode == modeFloorSelect){
            if(searchMode && searchedFloors.indexOf(mesh) == -1){
                return;
            }
            hoverOut(mesh);
            var selectedFloor;
            var nonSelectedFloors = {};
            for(var x in floorSet){
                if(floorSet[x].indexOf(mesh) >= 0){
                    selectedFloor = floorSet[x];
                }else{
                    nonSelectedFloors[x] = floorSet[x];
                }
            }
            selectedFloorMeshes = selectedFloor;
            if(!searchMode){
                var mat = {op: 1};
            }else{
                var mat = {op: 0.1};
            }
            inTween = true;
            tweenHoverOver = new TWEEN.Tween(mat)
                .to({op: 0}, opacitySpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonSelectedFloors){
                        for(var z in nonSelectedFloors[y]){
                            nonSelectedFloors[y][z].material.opacity = mat.op;
                        }
                    }
                }).onComplete( function(){
                    for(var y in nonSelectedFloors){
                        for(var z in nonSelectedFloors[y]){
                            nonSelectedFloors[y][z].position.y += 200;
                        }
                    }
                    inTween = false;
                    currentMode = modeRoomSelect;
                } ).start();

            tweenCamPos = new TWEEN.Tween(cameraCopy.position)
                .to(cameraPositionRoomSelect, 500)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                }).start();
//            scene.position.y -= 10;
//            scene.position.z -= 10;
//            scene.position.x -= 10;
            cameraLookAt(scene.position);
        }
    }
}

function deselectMesh(mesh, objects){
    if(mesh && objects){
        console.log("deselect");
        if(currentMode == modeRoomSelect){
            if(isMeshSelected){
                if(!searchMode){
                    var mat = {op: nonSelectedOpacity};
                    tweenDeselect = new TWEEN.Tween(mat)
                        .to({op: 1}, opacitySpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            for(var i=0;i<objects.length;i++){
                                if(objects[i] != mesh)
                                    objects[i].material.opacity = mat.op;
                            }
                        } );
                    tweenDeselect.start();
                }
                cameraLookAt({x:0,y:0,z:0});
                cameraFov();
                isMeshSelected = false;
            }
        }else if(currentMode == modeFloorSelect){
            var selectedFloor;
            var nonSelectedFloors = {};
            for(var x in floorSet){
                if(x == mesh){
                    selectedFloor = floorSet[x];
                }else{
                    nonSelectedFloors[x] = floorSet[x];
                }
            }
            for(var y in nonSelectedFloors){
                for(var z in nonSelectedFloors[y]){
                    nonSelectedFloors[y][z].position.y = 0;
                }
            }
            var mat = {op: 0};
            if(!searchMode){
                var matNew = {op: 1};
            }else{
                var matNew = {op: nonSelectedOpacity};
            }
            inTween = true;
            tweenHoverOver = new TWEEN.Tween(mat)
                .to(matNew, opacitySpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonSelectedFloors){
                        for(var z in nonSelectedFloors[y]){
                            if(!searchMode){
                                nonSelectedFloors[y][z].material.opacity = mat.op;
                            }else{
                                if(searchedMeshes.indexOf(nonSelectedFloors[y][z])<0){
                                    nonSelectedFloors[y][z].material.opacity = mat.op;
                                }else{
                                    nonSelectedFloors[y][z].material.opacity = 1;
                                }
                            }
                        }
                    }
                }).onComplete( function(){
                    inTween = false;
                } ).start();

            tweenCamPos = new TWEEN.Tween(cameraCopy.position)
                .to(cameraPositionFloorSelect, 500)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                }).start();
//            scene.position.y = -10;
//            scene.position.z = -10;
//            scene.position.x = -10;
            cameraLookAt(scene.position);

            for(var z in selectedFloor){
                selectedFloor[z].position.y = 3;
                selectedFloor[z].position.y = 0;
            }
        }
    }else{
        var tempCamPos;
        if(currentMode == modeFloorSelect){
            tempCamPos = cameraPositionFloorSelect;
        }else if(currentMode == modeRoomSelect){
            tempCamPos = cameraPositionRoomSelect;
        }
        tweenCamPos = new TWEEN.Tween(cameraCopy.position)
            .to(tempCamPos, 500)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate( function (){
            }).start();
        cameraLookAt(scene.position);
    }
}

function showSearchedMeshes(meshNames){
    searchMode = true;
    searchedMeshes = [];
    searchedFloors = [];
    for(var y in meshNames){
        if(roomNames.indexOf(meshNames[y]) != -1){
            searchedMeshes.push(meshNameSet[meshNames[y]]);
        }
        for(var xx in floorSet[meshNames[y].substring(0,4)]){
            searchedFloors.push(floorSet[meshNames[y].substring(0,4)][xx]);
        }
    }
    var mat = {op: 1};
    tweenSelect = new TWEEN.Tween(mat)
        .to({op: nonSelectedOpacity}, opacitySpeed)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate( function (){
                for(var i in allObjects){
//                    console.log(searchedMeshes.indexOf(allObjects[i]));
                    if(searchedMeshes.indexOf(allObjects[i]) < 0){
                        allObjects[i].material.opacity = mat.op;
                    }else{
                        allObjects[i].material.opacity = 1;
                    }
                }
        }).start();
}


function cameraLookAt(clickPos){
    if(clickPos){
        var defLook = defaultLookAt;
        tweenLookAt = new TWEEN.Tween(defLook)
            .to( clickPos , 500)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate( function (){
//                cameraCopy.lookAt(defLook);
                scene.position.x = defLook.x;
                scene.position.y = defLook.y;
                scene.position.z = defLook.z;
                cameraCopy.lookAt(scene.position);
            } );
        tweenLookAt.start();
    }
}

function cameraFov(zoomIn){
    var oldFov = { fov: cameraCopy.fov };
    var newFov;
    if(zoomIn){
//        vector between camera and click
//        var vec = {x: defaultCamPos.x-clickPos.x, y: defaultCamPos.y-clickPos.y, z: defaultCamPos.z-clickPos.z};
        newFov = { fov: defaultFoV -10 };
    }else{
        newFov = { fov: defaultFoV };
    }
    tweenFov = new TWEEN.Tween(oldFov)
        .to( newFov , 500)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate( function (){
            cameraCopy.fov = oldFov.fov;
        } );
    tweenFov.start();
}

// calculate the center of a mesh
function getCenter(mesh){
    if(mesh){
        mesh.geometry.computeBoundingBox();
        var boundingBox = mesh.geometry.boundingBox;

        var x = ((boundingBox.max.x - boundingBox.min.x)/2)+boundingBox.min.x;
        var y = ((boundingBox.max.y - boundingBox.min.y)/2)+boundingBox.min.y;
        var z = ((boundingBox.max.z - boundingBox.min.z)/2)+boundingBox.min.z;
        return {x: boundingBox.max.x, y: boundingBox.max.y, z: boundingBox.max.z};
    }else{
        return null;
    }
}