/**
 * Created by Nils Brinkmann on 12.12.13.
 */

$(function () {
    var webGL = $("#WebGL-output");
    var accordion = document.getElementById("accordion");
    $('#accordion').accordion({ heightStyle: "content" });
    var searchInput = $('#search-input');
    document.getElementById('search-input').value = "";

    var meshFiles;
    var geometryNameSet;
    var data = new Object();
    var activeRoomName;
    var activeFloorMeshes;
    var nameOfActiveFloor;
    var roomnamesOfActiveFloor = [];
    var meshNameSet = {};

    // WebSocket-Connection to Server
    var socket = io.connect('http://localhost:3000');

    // getting the geometrie-data from server
    socket.on('files', function(files){
        meshFiles = files;
        loadMeshes();
    });

    // getting the database-data from server
    socket.on('data', function(d){
        data = d;
        $('#accordion').accordion("refresh");
    });

    $( "#accordion" ).accordion();

    // refreshing the accordion panel
    function refreshAccordion(){
        if(searchMode){
            $( "#accordion" ).accordion({
                active: false,
                collapsible: true
            });
            return;
        }
        if(!data){
            alert("no db connection!");
        }
        $('#accordion').empty();
        roomnamesOfActiveFloor = [];
        for(var i=0;i<data.length;i++){
//            when floor is selected
            if(nameOfActiveFloor && data[i].room.substring(0,4) == nameOfActiveFloor){
                roomnamesOfActiveFloor.push(data[i].room);
                var room = document.createElement('h3');
                var r = data[i].room;
                room.innerHTML = r.substring(4);
                var schedule = document.createElement('div');
                var scheduleData = data[i].schedule;
                for(var k in scheduleData){
                    var p = document.createElement('p');

                    p.innerHTML = k + " : " + scheduleData[k];
                    schedule.appendChild(p);
                }
                document.getElementById("accordion").appendChild(room);
                document.getElementById("accordion").appendChild(schedule);
            }else{
                // toDo
                // when building is selected
            }
        }
        $( "#accordion" ).accordion({
            active: false,
            collapsible: true
        });
        $('#accordion').accordion("refresh");
    }

    // setting the active element in the accordion
    function setAccordionActive(param){
        var found;
        if(param == false){
            $( "#accordion" ).accordion( "option", "active", null );
            return;
        }
        found = false;
        var i = 0;
        if(!searchMode){
            for(i in roomnamesOfActiveFloor){
                if(roomnamesOfActiveFloor[i] == activeRoomName){
                    found = true;
                    break;
                }
            }
        }else{
            for(i in foundMeshNames){
                if(foundMeshNames[i] == activeRoomName){
                    found = true;
                    break;
                }
            }
        }
        if(found){
            $( "#accordion" ).accordion( "option", "active", parseInt(i) );
        }else{
            $( "#accordion" ).accordion( "option", "active", null );
        }
    }

    // search rooms from input field
    var foundMeshNames =[];
    function search(event){
        if(event.keyCode == 13){
            if(document.getElementById('search-input').value.trim() == ""){
                document.getElementById("reset").click();
                return;
            }
            document.getElementById("buildingBread").click();
            var searchString = document.getElementById('search-input').value.toLowerCase();
            if(searchMode){
                document.getElementById("reset").click();
                document.getElementById('search-input').value = searchString;
            }
            console.log(searchString);

            if(!data){
                alert("no db connection!");
            }
            $('#accordion').empty();
            var found = false;
            foundMeshNames =[];
            for(var i=0;i<data.length;i++){
                found = false;
                if((data[i].room.substring(4)).toLowerCase().indexOf(searchString) != -1){
                    found = true;
                }else{
                    var schedData = data[i].schedule;
                    for(var k in schedData){
                        if(schedData[k].toLowerCase().indexOf(searchString) != -1){
                            found = true;
                            break;
                        }
                    }
                }
                if(found){
                    var room = document.createElement('h3');
                    var r = data[i].room;
                    foundMeshNames.push(r);
                    room.innerHTML = r.substring(4);
                    var schedule = document.createElement('div');
                    var scheduleData = data[i].schedule;
                    for(var k in scheduleData){
                        var p = document.createElement('p');

                        p.innerHTML = k + " : " + scheduleData[k];
                        schedule.appendChild(p);
                    }
                    document.getElementById("accordion").appendChild(room);
                    document.getElementById("accordion").appendChild(schedule);
                }
            }
            $( "#accordion" ).accordion({
                active: false,
                collapsible: true
            });
            $('#accordion').accordion("refresh");

            if(foundMeshNames.length > 0){
                showSearchedMeshes(foundMeshNames);
            }
        }
    }


    var heightScale =  window.innerHeight/window.innerWidth;

    // loader that loads the geometries from JSON-files
    var loader = new THREE.JSONLoader();

//    var stats = initStats();
    var projector = new THREE.Projector();
    var raycaster = new THREE.Raycaster();

    var clickableObjects = [];
    var allObjects = [];
    var selectedMesh, hoveredMesh, hoveredRoom;
    var notClickable = [ "eg1-01", "og1-floor", "og2-Floor"];

    var roomNames = [];
    var floorSet = {};

    var mouse = new THREE.Vector2();

    // create a scene, that will hold all elements
    var scene = new THREE.Scene();
    setScene(scene);

    // create a camera
    var camera = new THREE.PerspectiveCamera(45, (webGL.width() ) / (webGL.width() * heightScale), 0.1, 1000);
    // set the position of the camera and point the camera to the center of the scene
    camera.position.x = 22;
    camera.position.y = 45;
    camera.position.z = 49;
    setCamera(camera);
    camera.lookAt(scene.position);

    // create a renderer and set the size
    var renderer;
    if( Detector.webgl ){
        renderer = new THREE.WebGLRenderer({
            antialias                : true       // to get smoother output
        });
        renderer.setClearColor(0xffffff, 1.0);
    }else{
        Detector.addGetWebGLMessage();
        return true;
    }
    renderer.setSize((webGL.width() ), (webGL.width() * heightScale));
    renderer.shadowMapEnabled = true;
    renderer.sortObjects = false;

    // add controls
    var controls = new THREE.OrbitControls(camera, renderer.domElement, scene);
    controls.noPan = false;
    controls.noKeys = true;

    // axis helper
    var axes = new THREE.AxisHelper( 20 );
//    scene.add(axes);

    // create light sources
    var pointLight = new THREE.PointLight(0x555555);
    pointLight.position.set(10,40,60);
    scene.add(pointLight);
    var pointLight2 = new THREE.PointLight(0x555555);
    pointLight2.position.set(-40,20,40);
    scene.add(pointLight2);
    var pointLight3 = new THREE.PointLight(0x777777);
    pointLight3.position.set(-10,40,-30);
    scene.add(pointLight3);

    var directionalLight = new THREE.SpotLight(0x111111);
    directionalLight.position.set(15,80,-20);
    var target = new THREE.Object3D();
    target.position = new THREE.Vector3(0,0,0);
    directionalLight.target = target;
    directionalLight.castShadow = true;
    directionalLight.onlyShadow = true;
    directionalLight.shadowDarkness = 0.6;
    directionalLight.shadowCameraVisible = false;
    directionalLight.shadowCameraFar = 100;
    directionalLight.shadowCameraNear = 50;

    directionalLight.shadowMapWidth = 1024;
    directionalLight.shadowMapHeight = 1024;

    scene.add(directionalLight);

    // create an basic material
    var basicMaterial = new THREE.MeshPhongMaterial({
        color: 0x0098A1,
//        color: 0x222222,
        transparent: true,
        opacity: 1,
        side: THREE.DoubleSide,
        shading: THREE.FlatShading
    });

    // iterate over all JSON-files
    function loadMeshes(){
        for(var i=0;i<meshFiles.length;i++){
            var mesh = "models/" + meshFiles[i];
            var tmp = meshFiles[i].split('.');
            var name = tmp[0];
            roomNames.push(name);
            if(notClickable.indexOf(name) >= 0){
                loader.load(mesh, function(geometry) {
                    createMesh( geometry, false);
                });
            }else{
                loader.load(mesh, function(geometry) {
                    createMesh( geometry);
                });
            }
        }
    }

    // ctreate a mesh object, add a material
    function createMesh( geometry, clickable){
        if(clickable == undefined){
            clickable = true;
        }
        var mesh;
        mesh = new THREE.Mesh(geometry, basicMaterial.clone());
        mesh.scale.x = 3;
        mesh.scale.y = 3;
        mesh.scale.z = 3;
        mesh.position.x = 8;
        mesh.receiveShadow = false;
        mesh.castShadow = true;
        scene.add(mesh);
        if(clickable){
            clickableObjects.push(mesh);
        }
        allObjects.push(mesh);
        if(allObjects.length == meshFiles.length){
            console.log("all files loaded");
            for(var i in roomNames){
                var floor = roomNames[i].substring(0,4);
                if(floorSet[floor]){
                    floorSet[floor].push(allObjects[i]);
                }else{
                    floorSet[floor] = [];
                    floorSet[floor].push(allObjects[i]);
                }
                meshNameSet[roomNames[i]] = allObjects[i];
            }
//            roomNames[allObjects.indexOf(selectedMesh)];
            setObjRooms(allObjects, roomNames, floorSet, meshNameSet);
        }
    }

    // adding the EventListeners
    var activeAccordionIndex;
    document.getElementById('WebGL-output').addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.getElementById('WebGL-output').addEventListener( 'mouseup', onDocumentMouseUp, false );
    document.getElementById('WebGL-output').addEventListener( 'mousemove', onDocumentMouseMove, false );
    window.addEventListener( 'resize', onWindowResize, false );
    document.getElementById('search-input').addEventListener( 'keyup', search, false );
    $( "#accordion" ).on( "accordionbeforeactivate", function( event, ui ) {
        if( ui.newHeader.text() != ""){
            console.log("tab opened: " + ui.newHeader.text());
            var headerText = ui.newHeader.text();
            for(var i in roomNames){
                if(roomNames[i].substring(4) == headerText){
                    if(nameOfActiveFloor == roomNames[i].substring(0, 4)){
                        onDocumentMouseUp("open", allObjects[i]);
                    }else if(nameOfActiveFloor==null || nameOfActiveFloor==undefined){
                        onDocumentMouseUp("open", allObjects[i]);
                    }
                }
            }
        }else{
            console.log("tab closed");
            var headerText = ui.oldHeader.text();
            console.log(headerText);
            for(var i in roomNames){
                if(roomNames[i].substring(4) == headerText){
                    onDocumentMouseUp("close", allObjects[i]);
                }
            }
            activeAccordionIndex = null;
        }
    });
    $( "#accordion" ).on( "accordionactivate", function( event, ui ) {
        var check = parseInt($(this).accordion('option', 'active'));
            if(check >= 0 ){
            }else{
                $("#accordion h3").removeClass('ui-state-focus');
            }
        });

    // refresh the camera and renderer size on resize of the browser window
    function onWindowResize() {
        heightScale =  window.innerHeight/window.innerWidth;
        $('#sidebar').height($('canvas').height());
            camera.aspect = (webGL.width()) / (webGL.width() * heightScale);
            camera.updateProjectionMatrix();

            renderer.setSize( (webGL.width()), (webGL.width() * heightScale) );
    }

    var mouseTmp = new THREE.Vector2();
    function onDocumentMouseDown(event){
        mouseTmp.x = ( (event.clientX - webGL.offset().left) / (webGL.width()) ) * 2 - 1;
        mouseTmp.y = - ( (event.clientY - webGL.offset().top) / (webGL.width() * heightScale) ) * 2 + 1;
    }

    // on release of mouse button
    var clickPoint;
    function onDocumentMouseUp( event, mesh ) {
        if(event && !mesh){
            // mouse position + offset if canvas is not at pos 0,0
            mouse.x = ( (event.clientX - webGL.offset().left) / (webGL.width()) ) * 2 - 1;
            mouse.y = - ( (event.clientY - webGL.offset().top) / (webGL.width() * heightScale) ) * 2 + 1;

            if(mouse.x == mouseTmp.x && mouse.y == mouseTmp.y){
                event.preventDefault();
                var vector = new THREE.Vector3( mouse.x, mouse.y, 0.5 );

                projector.unprojectVector( vector, camera );
                raycaster.set( camera.position, vector.sub( camera.position ).normalize() );

                if(getMode() == modeRoomSelect){
                    var clickIntersects = raycaster.intersectObjects( clickableObjects, true );
                    if ( clickIntersects.length > 0 ) {
                        if(isMeshClickable(clickIntersects[ 0 ].object)){
                            if(selectedMesh == clickIntersects[ 0 ].object){
                                deselectMesh(selectedMesh, allObjects);
                                hoverOut(selectedMesh);
                                selectedMesh = null;
//                                refreshAccordion();
                                setAccordionActive(false);
                                document.getElementById("breadcrumbs").removeChild(room);
                                document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
                            }else{
                                if(selectedMesh) {
                                    deselectMesh(selectedMesh, allObjects);
                                    hoverOut(selectedMesh);
                                    document.getElementById("breadcrumbs").removeChild(room);
                                    document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
                                }
                                selectedMesh = clickIntersects[ 0 ].object;
                                clickPoint = clickIntersects[0].point;
                                activeRoomName = roomNames[allObjects.indexOf(selectedMesh)];
                                console.log(activeRoomName);
                                addBreadcrumb("room", activeRoomName);
                                setAccordionActive();
                                selectMesh(selectedMesh, allObjects, clickPoint);
                            }
                        }
                    }else if( clickIntersects.length == 0 ){

                    }
                }else if(getMode() == modeFloorSelect){
                    var clickIntersects = raycaster.intersectObjects( allObjects, true );
                    if ( clickIntersects.length > 0 ) {
        //                activeRoomName = roomNames[allObjects.indexOf(clickIntersects[ 0 ].object)];
                        if(isFloorClickable(clickIntersects[ 0 ].object)){
                            for(var x in floorSet){
                                if(floorSet[x].indexOf(clickIntersects[ 0 ].object) >= 0){
                                    activeFloorMeshes = floorSet[x];
                                    nameOfActiveFloor = x;
                                    break;
                                }
                            }
                            refreshAccordion();
                            addBreadcrumb("floor", nameOfActiveFloor);
                            selectMesh(clickIntersects[ 0 ].object, allObjects, clickIntersects[0].point);
                        }
                    }
                }
            }
        }else if(mesh != selectedMesh && event == "open"){
            if(getMode() == modeFloorSelect){
                console.log("modeFloorSelect");
                for(var x in floorSet){
                    if(floorSet[x].indexOf(mesh) >= 0){
                        activeFloorMeshes = floorSet[x];
                        nameOfActiveFloor = x;
                        break;
                    }
                }
                addBreadcrumb("floor", nameOfActiveFloor);
                selectMesh(mesh, allObjects, true);
                setTimeout(function(){
                    setMode(modeRoomSelect);
                    selectedMesh = mesh;
                    clickPoint = getCenter(mesh);
                    activeRoomName = roomNames[allObjects.indexOf(selectedMesh)];
                    console.log(activeRoomName);
                    addBreadcrumb("room", activeRoomName);
                    hoverOver(selectedMesh);
                    selectMesh(selectedMesh, allObjects, clickPoint);
                }, 10);
            }else if(getMode() == modeRoomSelect){
                if(selectedMesh) {
                    deselectMesh(selectedMesh, allObjects);
                    hoverOut(selectedMesh);
                    document.getElementById("breadcrumbs").removeChild(room);
                    document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
                }
                selectedMesh = mesh;
                clickPoint = getCenter(mesh);
                activeRoomName = roomNames[allObjects.indexOf(selectedMesh)];
                console.log(activeRoomName);
                addBreadcrumb("room", activeRoomName);
                hoverOver(selectedMesh);
                selectMesh(selectedMesh, allObjects, clickPoint);
            }
        }else if(mesh == selectedMesh && event == "close"){
            deselectMesh(selectedMesh, allObjects);
            hoverOut(selectedMesh);
            selectedMesh = null;
            document.getElementById("breadcrumbs").removeChild(room);
            document.getElementById("breadcrumbs").removeChild(document.getElementById("floorToRoomArrow"));
        }
    }

    // if mouse cursor was moved
    function onDocumentMouseMove( event ) {
        event.preventDefault();

        // mouse position + offset if canvas is not at pos 0,0
        mouse.x = ( (event.clientX - webGL.offset().left) / (webGL.width()) ) * 2 - 1;
        mouse.y = - ( (event.clientY - webGL.offset().top) / (webGL.width() * heightScale) ) * 2 + 1;

        // find intersections for mouseOver
        var vector = new THREE.Vector3( mouse.x, mouse.y, 0 );

        projector.unprojectVector( vector, camera );
        raycaster.set( camera.position, vector.sub( camera.position ).normalize() );
        var mouseIntersects;
        if(getMode() == "room"){
            mouseIntersects = raycaster.intersectObjects( clickableObjects, true );
        }else if(getMode() == "floor"){
            mouseIntersects = raycaster.intersectObjects( allObjects, true );
        }else{
            mouseIntersects = raycaster.intersectObjects( clickableObjects, true );
        }

        if ( mouseIntersects.length > 0 ) {
            if ( hoveredMesh != mouseIntersects[ 0 ].object ) {
                if ( hoveredMesh ){
                    if(hoveredMesh != selectedMesh)
                        hoverOut(hoveredMesh);
                        for(var i = 0; i < $("#accordion h3").length; i++){
                            if($("#accordion h3").eq(i).text() == hoveredRoom){
                                $("#accordion h3").eq(i).removeClass('ui-state-hover');
                            }
                        }
                        hoveredRoom = null;

                }

                hoveredMesh = mouseIntersects[ 0 ].object;
                hoverOver(hoveredMesh);
                hoveredRoom = roomNames[allObjects.indexOf(mouseIntersects[ 0 ].object)].substring(4);
                for(var i = 0; i < $("#accordion h3").length; i++){
                    if($("#accordion h3").eq(i).text() == hoveredRoom){
                        $("#accordion h3").eq(i).addClass('ui-state-hover');
                    }
                }
            }
        } else {
            if ( hoveredMesh ){
                if(hoveredMesh != selectedMesh)
                    hoverOut(hoveredMesh);
                    for(var i = 0; i < $("#accordion h3").length; i++){
                        if($("#accordion h3").eq(i).text() == hoveredRoom){
                            $("#accordion h3").eq(i).removeClass('ui-state-hover');
                        }
                    }
                    hoveredRoom = null;
            }

            hoveredMesh = null;
        }
    }

    // adding the breadcrumbs to the 3D object
    var breadcrumbs = document.getElementById("breadcrumbs");
    var room, floor, building;
    function addBreadcrumb(mode, name){
        console.log("addBreadcumb, mode = "+mode);
        if(mode == modeRoomSelect){
            console.log("addBread room");
            if(!document.getElementById("floorToRoomArrow")){
                var floorToRoomArrow = document.createElement('span');
                floorToRoomArrow.id = "floorToRoomArrow";
                floorToRoomArrow.appendChild(document.createTextNode(' > '));
                breadcrumbs.appendChild(floorToRoomArrow);
            }

            room = document.createElement('button');
            room.id = "roomBread";
            room.className = "btn btn-default";
            room.appendChild(document.createTextNode(name.substring(4)));
            breadcrumbs.appendChild(room);
            document.getElementById("roomBread").addEventListener("click", function () {

            }, false);
        }else if(mode == modeFloorSelect){
            console.log("addBread floor");
            var buildingToFloorArrow = document.createElement('span');
            buildingToFloorArrow.id = "buildingToFloorArrow";
            buildingToFloorArrow.appendChild(document.createTextNode(' > '));
            breadcrumbs.appendChild(buildingToFloorArrow);

            floor = document.createElement('button');
            floor.id = "floorBread";
            floor.className = "btn btn-default";
            floor.appendChild(document.createTextNode(name.substring(0,name.length-1).toUpperCase()));
            breadcrumbs.appendChild(floor);
            document.getElementById("floorBread").addEventListener("click", function () {
                deselectMesh(selectedMesh, allObjects);
                hoverOut(selectedMesh);
                selectedMesh = null;
                refreshAccordion();
                if(document.getElementById("roomBread")){
                    breadcrumbs.removeChild(document.getElementById("roomBread"));
                    breadcrumbs.removeChild(document.getElementById("floorToRoomArrow"));
                }
            }, false);
        }else if(mode == modeBuildingSelect){
            building = document.createElement('button');
            building.id = "buildingBread";
            building.className = "btn btn-default";
            building.appendChild(document.createTextNode(name));
            breadcrumbs.appendChild(building);
            document.getElementById("buildingBread").addEventListener("click", function () {
                if(selectedMesh){
                    deselectMesh(selectedMesh, allObjects);
                    hoverOut(selectedMesh);
                    selectedMesh = null;
                    document.getElementById("breadcrumbs").removeChild(room);
                }
                setMode(modeFloorSelect);
                deselectMesh(nameOfActiveFloor, allObjects);
                refreshAccordion();
                if(document.getElementById("floorBread")){
                    breadcrumbs.removeChild(document.getElementById("floorBread"));
                    breadcrumbs.removeChild(document.getElementById("buildingToFloorArrow"));
                }
            }, false);
        }
    }

    $(function(){
        console.log("adding first breadcrumb");
        building = document.createElement('button');
        building.id = "buildingBread";
//        building.type = "button";
        building.className = "btn btn-default";
        building.appendChild(document.createTextNode("Grashof"));
        breadcrumbs.appendChild(building);
        document.getElementById("buildingBread").addEventListener("click", function () {
            if(selectedMesh){
                deselectMesh(selectedMesh, allObjects);
                hoverOut(selectedMesh);
                selectedMesh = null;
                breadcrumbs.removeChild(document.getElementById("roomBread"));
                breadcrumbs.removeChild(document.getElementById("floorToRoomArrow"));
            }
            setMode(modeFloorSelect);
            deselectMesh(nameOfActiveFloor, allObjects);
            nameOfActiveFloor = null;
            refreshAccordion();
            if(document.getElementById("floorBread")){
                breadcrumbs.removeChild(document.getElementById("floorBread"));
                breadcrumbs.removeChild(document.getElementById("buildingToFloorArrow"));
            }
        }, false);
    });

    function renderScene(){
        requestAnimationFrame(renderScene);
        camera.updateProjectionMatrix();
        controls.update();
//        stats.update();
        TWEEN.update();

        renderer.render(scene, camera);
    }

    function initStats(){
        var stats = new Stats();
        stats.setMode(0);
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';
        $("#Stats-output").append(stats.domElement);
        return stats;
    }

    // add the output of the renderer to the html element
    webGL.append(renderer.domElement);
    renderScene();
    $('#sidebar').height(window.innerHeight-100);

    $(function(){
        document.getElementById("reset").addEventListener("click", function () {
            if(searchMode){
                document.getElementById("buildingBread").click();
                var mat = {op: 0.1};
                tweenSelect = new TWEEN.Tween(mat)
                    .to({op: 1}, opacitySpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        for(var i in allObjects){
                            allObjects[i].material.opacity = mat.op;
                        }
                    }).start();
                searchMode = false;
                foundMeshNames = [];
                document.getElementById('search-input').value = "";
                refreshAccordion();
            }
        }, false);
    });
});