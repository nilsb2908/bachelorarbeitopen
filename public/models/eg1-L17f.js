{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.65 Exporter",
		"vertices"      : 14,
		"faces"         : 8,
		"normals"       : 12,
		"colors"        : 0,
		"uvs"           : [],
		"materials"     : 0,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [],

	"vertices" : [-7.51675,-1.95011e-06,-2.57563,-8.33196,-1.95011e-06,-2.57563,-7.51675,-1.97574e-06,-3.16207,-8.33196,-1.97574e-06,-3.16207,-8.33196,-1.96911e-06,-3.0104,-7.51675,-1.95877e-06,-2.77375,-7.51675,-1.96761e-06,-2.976,-7.51675,0.499998,-2.57563,-8.33196,0.499998,-2.57563,-7.51675,0.499998,-3.16207,-8.33196,0.499998,-3.16207,-8.33196,0.499998,-3.0104,-7.51675,0.499998,-2.77375,-7.51675,0.499998,-2.976],

	"morphTargets" : [],

	"normals" : [-0.577349,0.577349,0.577349,-0.447188,0.894406,0,-1,0,0,-0.707083,0,0.707083,0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,0.447188,0.894406,0,-0.707083,0,-0.707083,0,0.707083,0.707083,1,0,0,0.707083,0,-0.707083,0,0,1],

	"colors" : [],

	"uvs" : [[]],

	"faces" : [33,2,6,13,9,0,1,2,3,33,1,0,5,4,4,5,1,6,33,5,0,7,12,1,5,7,2,32,3,6,2,8,1,0,33,4,5,6,3,6,1,1,8,33,1,4,11,8,4,6,9,10,33,3,2,9,10,8,0,3,11,33,0,1,8,7,5,4,10,7],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animations" : []


}
