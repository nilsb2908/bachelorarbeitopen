
var fs = require('fs');

exports.index = function(req, res){
  res.render('grashof');
};
exports.contact = function(req, res){
    res.render('contact');
};

exports.about = function(req, res){
    res.render('about');
};

exports.roomplans = function(db){
    return function(req, res) {
        var collection = db.get('Roomplans');
        collection.find({},{},function(e,docs){
            res.render('roomplans', {
                "roomplans" : docs
            });
        });
    };
};

exports.lndw = function(req, res){
    res.render('lndw');
};

