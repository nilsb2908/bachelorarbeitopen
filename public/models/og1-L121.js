{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.65 Exporter",
		"vertices"      : 16,
		"faces"         : 10,
		"normals"       : 12,
		"colors"        : 0,
		"uvs"           : [],
		"materials"     : 0,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [],

	"vertices" : [-14.3774,0.7,0.589516,-15.6598,0.7,0.589516,-14.3774,0.7,-1.06465,-15.6598,0.7,-1.06465,-15.6598,0.7,-0.592653,-14.3774,0.7,-0.592653,-15.6598,0.7,-0.397074,-14.3774,0.7,-0.397074,-14.3774,1.2,0.589516,-15.6598,1.2,0.589516,-14.3774,1.2,-1.06465,-15.6598,1.2,-1.06465,-15.6598,1.2,-0.592653,-14.3774,1.2,-0.592653,-15.6598,1.2,-0.397074,-14.3774,1.2,-0.397074],

	"morphTargets" : [],

	"normals" : [0.707083,0.707083,0,-0.447188,0.894406,0,-0.577349,0.577349,0.577349,0.577349,0.577349,0.577349,0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,1,0,0,-0.707083,0,-0.707083,-1,0,0,0.707083,0,0.707083,-0.707083,0,0.707083,0.707083,0,-0.707083],

	"colors" : [],

	"uvs" : [[]],

	"faces" : [33,4,5,2,3,0,1,2,3,33,6,7,5,4,0,1,1,0,33,1,0,7,6,4,5,1,0,33,6,4,12,14,0,0,6,6,33,7,0,8,15,1,5,7,8,33,4,3,11,12,0,3,9,6,33,2,5,13,10,2,1,8,10,33,1,6,14,9,4,0,6,11,33,3,2,10,11,3,2,10,9,33,0,1,9,8,5,4,11,7],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animations" : []


}
