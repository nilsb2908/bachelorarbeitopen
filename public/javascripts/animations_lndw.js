/**
 * Created by Nils Brinkmann on 12.12.13.
 */

var scene;

var cameraCopy;
var cameraPositionBuilding = { };
var cameraPositionFloorSelect = { x: 24.7, y: 19.8, z: 68.1 };
var cameraPositionRoomSelect = { x: 22, y: 37, z: 45 };

var modeBuildingSelect = "building";
var modeFloorSelect = "floor"
var modeRoomSelect = "room";
var currentMode = modeFloorSelect;
var searchMode = false;

var maxHoverHeight = 0.4;
var nonSelectedOpacity = 0.2;
var nonSelectedFloorOpacity = 0.3;
var hoverSpeed = 500;
var opacitySpeed = 500;
var allObjects, roomNames, floorSet = [], meshNameSet, selectedFloorMeshes = [], standMeshes = [];

var defaultRoomLookAt = {x: 0, y: 10, z: 0};
var defaultFoV;

var isMeshSelected = false;
var selectedMesh;
var inTween = false;

var tweenHoverOver, tweenHoverOver2, tweenHoverOut, tweenHoverOut2, tweenSelect, tweenDeselect;

var searchedMeshes = [];
var searchedFloors = [];

// camera tweens
var tweenLookAt, tweenFov, tweenCamPos;

function setScene(sc){
    scene = sc;
}

function setObjRooms(objects, rooms, fset, rset){
    allObjects = objects;
    roomNames = rooms;
    floorSet = fset;
    meshNameSet = rset;
}

function setCamera(cam){
    if(!cameraCopy){
        cameraCopy = cam;
        defaultFoV = cameraCopy.clone().fov;
    }
    if(currentMode == modeRoomSelect){
        cameraCopy.position.x = cameraPositionRoomSelect.x;
        cameraCopy.position.y = cameraPositionRoomSelect.y;
        cameraCopy.position.z = cameraPositionRoomSelect.z;
    }else if(currentMode == modeFloorSelect){
        cameraCopy.position.x = cameraPositionFloorSelect.x;
        cameraCopy.position.y = cameraPositionFloorSelect.y;
        cameraCopy.position.z = cameraPositionFloorSelect.z;
        scene.position.y += 10;
    }else if(currentMode == modeBuildingSelect){
//        cameraCopy.position.x = cameraPositionBuilding.x;
//        cameraCopy.position.y = cameraPositionBuilding.y;
//        cameraCopy.position.z = cameraPositionBuilding.z;
    }
}

function isMeshClickable(mesh){
    if(searchMode && searchedMeshes.indexOf(mesh) == -1){
        return false;
    }
    return true;
}

function isFloorClickable(mesh){
    if(searchMode && searchedFloors.indexOf(mesh) == -1){
        return false;
    }
    return true;
}

function setMode(mode){
    currentMode = mode;
}

function getStandMeshes(){
    return standMeshes;
}

function getMode(){
    return currentMode;
}
function setSearchMode(bool){
    searchMode = bool;
}

function hoverOver(mesh){
    if(mesh && !inTween){
        var mat;
        var matNew;
        if(currentMode == modeRoomSelect){
            if(selectedFloorMeshes.indexOf(mesh) < 0){
                return;
            }
            var pos = {y: mesh.position.y};
            if(mesh != selectedMesh){
                if(!searchMode){
                    tweenHoverOver = new TWEEN.Tween(pos)
                        .to({y: maxHoverHeight}, hoverSpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            mesh.position.y = pos.y;
                        });
                    //            setTimeout('tweenHoverOver.start()', 500);
                    tweenHoverOver.start();
                }else{
                    if(searchedMeshes.indexOf(mesh) >= 0){
                        tweenHoverOver = new TWEEN.Tween(pos)
                            .to({y: maxHoverHeight}, hoverSpeed)
                            .easing(TWEEN.Easing.Quadratic.Out)
                            .onUpdate( function (){
                                mesh.position.y = pos.y;
                            });
                        //            setTimeout('tweenHoverOver.start()', 500);
                        tweenHoverOver.start();
                    }
                }
            }
        }else if(currentMode == modeFloorSelect){
            if(searchMode && searchedFloors.indexOf(mesh) == -1){
                return;
            }
            var hoveredFloor;
            var nonHoveredFloors = {};
            var nonHoveredMesh;
            for(var x in floorSet){
                if(floorSet[x].indexOf(mesh) >= 0){
                    hoveredFloor = floorSet[x];
                }else{
                    nonHoveredFloors[x] = floorSet[x];
                    nonHoveredMesh = nonHoveredFloors[x][1];
                }
            }
            tweenHoverOver = new TWEEN.Tween(mesh.scale)
                .to({x: 3.05, y: 3, z: 3.05}, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in hoveredFloor){
                        hoveredFloor[y].scale = mesh.scale;
                    }
                }).start();
            if(!searchMode){
                mat = {op: nonHoveredMesh.material.opacity};
                matNew = {op: nonSelectedFloorOpacity};
            }else{
                mat = {op: nonSelectedFloorOpacity};
                matNew = {op: 0.1};
            }
            tweenHoverOver2 = new TWEEN.Tween(mat)
                .to(matNew, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonHoveredFloors){
                        for(var z in nonHoveredFloors[y]){
                            if(!searchMode){
                                nonHoveredFloors[y][z].material.opacity = mat.op;
                            }else if(searchedMeshes.indexOf(nonHoveredFloors[y][z]) == -1){
                                nonHoveredFloors[y][z].material.opacity = mat.op;
                            }
                        }
                    }
                } );
            tweenHoverOver2.start();
        }
    }
}

function hoverOut(mesh){
    if(mesh && !inTween){
        var mat;
        var matNew;
        if(currentMode == modeRoomSelect){
            if(selectedFloorMeshes.indexOf(mesh) < 0){
                return;
            }
            var pos = {y: mesh.position.y};
            if(!searchMode){
                tweenHoverOut = new TWEEN.Tween(pos)
                    .to({y: 0}, hoverSpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        mesh.position.y = pos.y;
                    } );

                tweenHoverOut.start();
            }else{
                if(searchedMeshes.indexOf(mesh) >= 0){
                    tweenHoverOut = new TWEEN.Tween(pos)
                        .to({y: 0}, hoverSpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            mesh.position.y = pos.y;
                        } );

                    tweenHoverOut.start();
                }
            }
        }else if(currentMode == modeFloorSelect){
            if(searchMode && searchedFloors.indexOf(mesh) == -1){
                return;
            }
            var hoveredFloor;
            var nonHoveredFloors = {};
            var nonHoveredMesh;
            for(var x in floorSet){
                if(floorSet[x].indexOf(mesh) >= 0){
                    hoveredFloor = floorSet[x];
                }else{
                    nonHoveredFloors[x] = floorSet[x];
                    nonHoveredMesh = nonHoveredFloors[x][1];
                }
            }
            tweenHoverOut = new TWEEN.Tween(mesh.scale)
                .to({x: 3, y: 3, z: 3}, hoverSpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in hoveredFloor){
                        hoveredFloor[y].scale = mesh.scale;
                    }
                }).start();
            if(!searchMode){
                matNew = {op: 1};
//                var matNew = {op: nonHoveredMesh.material.opacity};
                mat = {op: nonSelectedFloorOpacity};
                tweenHoverOut2 = new TWEEN.Tween(mat)
                    .to(matNew, hoverSpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        for(var y in nonHoveredFloors){
                            for(var z in nonHoveredFloors[y]){
                                if(!searchMode){
                                    nonHoveredFloors[y][z].material.opacity = mat.op;
                                }else if(searchedMeshes.indexOf(nonHoveredFloors[y][z]) == -1){
                                    nonHoveredFloors[y][z].material.opacity = mat.op;
                                }
                            }
                        }
                    }).start();
            }else{
                matNew = {op: nonSelectedFloorOpacity};
                var matNew2 = {op: 1};
                mat = {op: 0.1};
                var mat2 = {op: 0.1};

                // for every floor with searched meshes
                new TWEEN.Tween(mat2)
                    .to(matNew2, hoverSpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        for(var y in searchedFloors){
                            if(hoveredFloor.indexOf(searchedFloors[y]) == -1){
                                searchedFloors[y].material.opacity = mat2.op;
                            }
                        }
                    }).start();
                // for every floor without searched meshes
                tweenHoverOut2 = new TWEEN.Tween(mat)
                    .to(matNew, hoverSpeed)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .onUpdate( function (){
                        for(var y in nonHoveredFloors){
                            for(var z in nonHoveredFloors[y]){
                                if(searchedFloors.indexOf(nonHoveredFloors[y][z]) == -1){
                                    nonHoveredFloors[y][z].material.opacity = mat.op;
                                }
                            }
                        }
                    } );
                tweenHoverOut2.start();
            }
        }
    }
}

function selectMesh(mesh, objects, clickPos){
    if(mesh && objects && clickPos){
        console.log("select");
        var mat;
        if(currentMode == modeRoomSelect){
            if(!isMeshSelected){
                if(!searchMode){
                    mat = {op: mesh.material.opacity};
                    if(mat.op < 1){
                        tweenSelect = new TWEEN.Tween(mat)
                            .to({op: 1}, opacitySpeed)
                            .easing(TWEEN.Easing.Quadratic.Out)
                            .onUpdate( function (){
                                mesh.material.opacity = mat.op;
                            } );
                        tweenSelect.start();
                    }
                    tweenSelect = new TWEEN.Tween(mat)
                        .to({op: nonSelectedOpacity}, opacitySpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            for(var i=0;i<objects.length;i++){
                                if(objects[i] != mesh)
                                    objects[i].material.opacity = mat.op;
                            }
                        } );
                    tweenSelect.start();
                }else{
                    mesh.material.opacity = 1;
                    var x = {op: 1};
                    tweenSelect = new TWEEN.Tween(x)
                        .to({op: 0.2}, opacitySpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            for(var y in searchedMeshes){
                                if(searchedMeshes[y] != mesh){
                                    searchedMeshes[y].material.opacity = x.op;
                                }
                            }
                        } );
                    tweenSelect.start();
                }
//                cameraLookAt(getCenter(mesh));
                clickPos = getCenter(mesh);
                clickPos.x += mesh.position.x;
                clickPos.y += mesh.position.y;
                clickPos.z += mesh.position.z;
                cameraLookAt(clickPos);
                cameraFov(true);
                isMeshSelected = true;
                selectedMesh = mesh;
                hoverOut(mesh);

//                console.log("clickPos: " + JSON.stringify(clickPos));
//                console.log("mesh.position: " + JSON.stringify(mesh.position));
////                console.log("mesh.vertices: " + JSON.stringify(mesh.geometry));
//                console.log("getCenter: " + JSON.stringify(getCenter(mesh)));
//                console.log("object.matrixWorld: " + JSON.stringify(new THREE.Vector3().getPositionFromMatrix(mesh.matrix)));
            }
        }else if(currentMode == modeFloorSelect){
            if(searchMode && searchedFloors.indexOf(mesh) == -1){
                return;
            }
            inTween = true;
            hoverOut(mesh);
            var selectedFloor;
            var nonSelectedFloors = {};
            for(var x in floorSet){
                if(floorSet[x].indexOf(mesh) >= 0){
                    selectedFloor = floorSet[x];
                    console.log("selectedFloor"+x);
                }else{
                    nonSelectedFloors[x] = floorSet[x];
                }
            }
            selectedFloorMeshes = selectedFloor;
            if(!searchMode){
                mat = {op: 1};
            }else{
                mat = {op: 0.1};
            }
            console.log("selectedFloorlength"+selectedFloor.length);
            tweenHoverOver = new TWEEN.Tween(mat)
                .to({op: 0}, opacitySpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonSelectedFloors){
                        for(var z in nonSelectedFloors[y]){
                            nonSelectedFloors[y][z].material.opacity = mat.op;
                        }
                    }
                }).onComplete( function(){
                    for(var y in nonSelectedFloors){
                        for(var z in nonSelectedFloors[y]){
                            nonSelectedFloors[y][z].position.y += 200;
                        }
                    }
                    inTween = false;
                    currentMode = modeRoomSelect;
                } ).start();

            tweenCamPos = new TWEEN.Tween(cameraCopy.position)
                .to(cameraPositionRoomSelect, 500)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                }).start();

            cameraLookAt(scene.position);
        }
    }
}

function deselectMesh(mesh, objects){
    if(mesh && objects){
        console.log("deselect");
        var mat;
        var matNew;
        if(currentMode == modeRoomSelect){
            if(isMeshSelected){
                if(!searchMode){
                    mat = {op: nonSelectedOpacity};
                    tweenDeselect = new TWEEN.Tween(mat)
                        .to({op: 1}, opacitySpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            for(var i=0;i<objects.length;i++){
                                if(objects[i] != mesh)
                                    objects[i].material.opacity = mat.op;
                            }
                        } );
                    tweenDeselect.start();
                }else{
                    mesh.material.opacity = 1;
                    var x = {op: 0.2};
                    tweenSelect = new TWEEN.Tween(x)
                        .to({op: 1}, opacitySpeed)
                        .easing(TWEEN.Easing.Quadratic.Out)
                        .onUpdate( function (){
                            for(var y in searchedMeshes){
                                if(searchedMeshes[y] != mesh){
                                    searchedMeshes[y].material.opacity = x.op;
                                }
                            }
                        } );
                    tweenSelect.start();
                }
                cameraLookAt({x:0,y:0,z:0});
                cameraFov();
                isMeshSelected = false;
                selectedMesh = null;
            }
        }else if(currentMode == modeFloorSelect){
            var selectedFloor;
            var nonSelectedFloors = {};
            for(var x in floorSet){
                if(x == mesh){
                    selectedFloor = floorSet[x];
                }else{
                    nonSelectedFloors[x] = floorSet[x];
                }
            }
            for(var y in nonSelectedFloors){
                for(var z in nonSelectedFloors[y]){
                    nonSelectedFloors[y][z].position.y = 0;
                }
            }
            mat = {op: 0};
            if(!searchMode){
                matNew = {op: 1};
            }else{
                matNew = {op: nonSelectedOpacity};
            }
            inTween = true;
            tweenHoverOver = new TWEEN.Tween(mat)
                .to(matNew, opacitySpeed)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                    for(var y in nonSelectedFloors){
                        for(var z in nonSelectedFloors[y]){
                            if(!searchMode){
                                nonSelectedFloors[y][z].material.opacity = mat.op;
                            }else{
                                if(searchedFloors.indexOf(nonSelectedFloors[y][z])<0){
                                    nonSelectedFloors[y][z].material.opacity = mat.op;
                                }else{
                                    nonSelectedFloors[y][z].material.opacity = 1;
                                }
                            }
                        }
                    }
                }).onComplete( function(){
                    inTween = false;
                } ).start();

            tweenCamPos = new TWEEN.Tween(cameraCopy.position)
                .to(cameraPositionFloorSelect, 500)
                .easing(TWEEN.Easing.Quadratic.Out)
                .onUpdate( function (){
                }).start();
//            scene.position.y = -10;
//            scene.position.z = -10;
//            scene.position.x = -10;
            cameraLookAt(scene.position);

            for(var z in selectedFloor){
                selectedFloor[z].position.y = 3;
                selectedFloor[z].position.y = 0;
            }
        }
    }else{
        var tempCamPos;
        if(currentMode == modeFloorSelect){
            tempCamPos = cameraPositionFloorSelect;
        }else if(currentMode == modeRoomSelect){
            tempCamPos = cameraPositionRoomSelect;
        }
        tweenCamPos = new TWEEN.Tween(cameraCopy.position)
            .to(tempCamPos, 500)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate( function (){
            }).start();
        cameraLookAt(scene.position);

    }
}

function showSearchedMeshes(meshNames){
    searchMode = true;
    searchedMeshes = [];
    searchedFloors = [];
    standMeshes = [];
    for(var y in meshNames){
        for(var z in roomNames){
            if(roomNames[z].indexOf("stand") != -1){
                standMeshes.push(meshNameSet[roomNames[z]]);
            }
            if(roomNames[z].substring(10) == meshNames[y].substring(4)){
                searchedMeshes.push(meshNameSet[roomNames[z]]);
                for(var xx in floorSet[roomNames[z].substring(0,4)]){
                    searchedFloors.push(floorSet[roomNames[z].substring(0,4)][xx]);
                }
            }
        }
    }
    var mat = {op: 1};
    tweenSelect = new TWEEN.Tween(mat)
        .to({op: 0.2}, opacitySpeed)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate( function (){
            for(var i in standMeshes){
//                    console.log(searchedMeshes.indexOf(allObjects[i]));
                if(searchedMeshes.indexOf(standMeshes[i]) < 0){
                    standMeshes[i].material.opacity = mat.op;
                    standMeshes[i].material.color = 0x0098A1;
                }else{
                    standMeshes[i].material.opacity = 1;
                }
            }
            for(var v in allObjects){
                if(standMeshes.indexOf(allObjects[v]) == -1 && searchedFloors.indexOf(allObjects[v]) == -1){
                    allObjects[v].material.opacity = mat.op;
                }
            }
        }).start();
}

function selectSearchedMesh(){

}

function cameraLookAt(clickPos){
    if(clickPos){
        var defLook = defaultRoomLookAt;
        tweenLookAt = new TWEEN.Tween(defLook)
            .to( clickPos , 500)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate( function (){
//                cameraCopy.lookAt(defLook);
                scene.position.x = defLook.x;
                scene.position.y = defLook.y;
                scene.position.z = defLook.z;
                cameraCopy.lookAt(scene.position);
            } );
        tweenLookAt.start();
    }
}

function cameraFov(zoomIn){
    var oldFov = { fov: cameraCopy.fov };
    var newFov;
    if(zoomIn){
//        vector between camera and click
//        var vec = {x: defaultCamPos.x-clickPos.x, y: defaultCamPos.y-clickPos.y, z: defaultCamPos.z-clickPos.z};
        newFov = { fov: defaultFoV -10 };
    }else{
        newFov = { fov: defaultFoV };
    }
    tweenFov = new TWEEN.Tween(oldFov)
        .to( newFov , 500)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate( function (){
            cameraCopy.fov = oldFov.fov;
        } );
    tweenFov.start();
}


function getCenter(mesh){
    if(mesh){
        mesh.geometry.computeBoundingBox();
        var boundingBox = mesh.geometry.boundingBox;

        var x = ((boundingBox.max.x - boundingBox.min.x)/2)+boundingBox.min.x;
        var y = ((boundingBox.max.y - boundingBox.min.y)/2)+boundingBox.min.y;
        var z = ((boundingBox.max.z - boundingBox.min.z)/2)+boundingBox.min.z;
        return {x: boundingBox.max.x, y: boundingBox.max.y, z: boundingBox.max.z};
    }else{
        return null;
    }
}