
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var THREE = require('three');
var fs = require('fs');
var routes = require('./routes');

var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/BeuthRoomplans');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/contact', routes.contact);
app.get('/lndw', routes.lndw);
app.get('/about', routes.about);
app.get('/roomplans', routes.roomplans(db));

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var fs = require('fs');

var files = fs.readdirSync(path.join('public', 'models'));
var filesLndw = fs.readdirSync(path.join('public', 'models_lndw'));
var allFiles = [];
for(var y in files){
    allFiles.push(files[y]);
    if(y == files.length - 1){
        for(var i in filesLndw){
            allFiles.push(filesLndw[i]);
        }
    }
}

//var loader = new THREE.JSONLoader();
//var meshNameSet = {};
//for(var i=0;i<files.length;i++){
//    var mesh = path.join('models', files[i])
////    var mesh = "models/" + files[i];
//    var tmp = files[i].split('.');
//    var name = tmp[0];
//    fs.readFile(mesh, 'utf8', function (err, data) {
//        if (err) {
//            console.log('Error: ' + err);
//            return;
//        }
//        data = JSON.parse(data);
//        var model = loader.parse( data );
//        var mesh = new THREE.Mesh( model.geometry, new THREE.MeshLambertMaterial({
//                color: 0x0098A1,
//    //        color: 0xdddddd,
//                transparent: true,
//                opacity: 1,
//                side: THREE.DoubleSide
//            })
//        );
//        mesh.scale.x = 3;
//        mesh.scale.y = 3;
//        mesh.scale.z = 3;
//        mesh.position.x = 8;
//        mesh.receiveShadow = false;
//        mesh.castShadow = true;
//        meshNameSet[name] = mesh;
//        });
//}

var roomplansData;
var roomplans = db.get('Roomplans');
roomplans.find({},{},function(e,docs){
    roomplansData = docs;
});
var lndwData;
var lndw = db.get('lndw');
lndw.find({},{},function(e,docs){
    lndwData = docs;
});

var io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket){
   socket.emit('files', files );
   socket.emit('data', roomplansData);
   socket.emit('filesLndw', allFiles);
    socket.emit('dataLndw', lndwData);
   console.log("Server send...");
});
