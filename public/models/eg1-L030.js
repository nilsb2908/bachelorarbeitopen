{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.65 Exporter",
		"vertices"      : 16,
		"faces"         : 10,
		"normals"       : 12,
		"colors"        : 0,
		"uvs"           : [],
		"materials"     : 0,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [],

	"vertices" : [-2.72777,-1.99316e-06,-3.85803,-4.85267,-1.99316e-06,-3.85803,-2.72777,-2.05645e-06,-5.30584,-4.85267,-2.05645e-06,-5.30584,-4.85267,-2.04264e-06,-4.98995,-2.72777,-2.04264e-06,-4.98995,-4.85267,-2.03631e-06,-4.84517,-2.72777,-2.03631e-06,-4.84517,-2.72777,0.499998,-3.85803,-4.85267,0.499998,-3.85803,-2.72777,0.499998,-5.30584,-4.85267,0.499998,-5.30584,-4.85267,0.499998,-4.98995,-2.72777,0.499998,-4.98995,-4.85267,0.499998,-4.84517,-2.72777,0.499998,-4.84517],

	"morphTargets" : [],

	"normals" : [0.447188,0.894406,0,-0.707083,0.707083,0,-0.577349,0.577349,0.577349,0.577349,0.577349,0.577349,0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-1,0,0,-0.707083,0,-0.707083,0.707083,0,0.707083,1,0,0,-0.707083,0,0.707083,0.707083,0,-0.707083],

	"colors" : [],

	"uvs" : [[]],

	"faces" : [33,4,5,2,3,0,1,2,3,33,6,7,5,4,0,1,1,0,33,1,0,7,6,4,5,1,0,33,5,7,15,13,1,1,6,6,33,7,0,8,15,1,5,7,6,33,4,3,11,12,0,3,8,9,33,2,5,13,10,2,1,6,10,33,1,6,14,9,4,0,9,11,33,3,2,10,11,3,2,10,8,33,0,1,9,8,5,4,11,7],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animations" : []


}
